import React, { Component } from 'react';
import Search from './Search';
import Results from './Results';
import './App.css';

class App extends Component {
    constructor(props){
      super(props);
      this.state = {
        results: []
      }
    }
  render() {
    return (
      <div className="App">
        <div className="app-header">
          <Search value={this.state.value} />

        </div>
        <div className="app-results">
          <Results results={this.state.results} />
        </div>
      </div>
    );
  }

}

export default App;
