import React from 'react';

class Search extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			value: "",
			results: []
		}

		this.handleChange = this.handleChange.bind(this);
		this.getSearchResults = this.getSearchResults.bind(this);
	}

	handleChange(e){
		this.setState({value: e.target.value})
	}

	getSearchResults(e){
		var self = this;
		if(e.keyCode === 13){
			//ajax
			var url = "http://api.duckduckgo.com/?q="+ this.state.value + "&format=json";
			var xhr = new XMLHttpRequest();

			xhr.onreadystatechange = function(){
				if(this.readyState === 4 && this.status === 200){
					var json = JSON.parse(xhr.responseText);
					console.log(json.RelatedTopics)
					var newArray  = [];
					json.RelatedTopics.forEach(function(item, key){
						newArray.push(item);
					});
					self.setState({results: newArray});
				}

			}

			xhr.open("GET", url, true);
			//xhr.setRequestHeader("Access-Control-Allow-Origin", "")
			xhr.send();
		}
	};
	render() {
	  return <input type="text" value={this.state.value} className="searchBox" autoFocus="true" tabIndex="0" onChange={this.handleChange} onKeyDown={this.getSearchResults} />;

	}
}
export default Search;